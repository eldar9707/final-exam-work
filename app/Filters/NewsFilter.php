<?php

namespace App\Filters;

class NewsFilter extends QueryFilter
{
    /**
     * @param $id
     * @return mixed
     */
    public function tag_id($id)
    {
        return $this->builder
            ->orWhereHas('tags', function($tags) use ($id) {
                $tags->where('tag_id', $id);
            });
    }

    /**
     * @param $id
     * @return mixed
     */
    public function category_id($id)
    {
        return $this->builder
            ->where('category_id', $id);
    }
}
