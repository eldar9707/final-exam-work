<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Auth::user() and \Auth::user()->is_admin or \Auth::user() and \Auth::user()->is_editor) {
            return $next($request);
        }

        return redirect('/');
    }
}
