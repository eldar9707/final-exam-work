<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Models\Rating;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * @param RatingRequest $request
     * @return RedirectResponse
     */
    public function store(RatingRequest $request): RedirectResponse
    {
        $rating = new Rating($request->validated());
        $rating->save();

        return redirect()->back();
    }
}
