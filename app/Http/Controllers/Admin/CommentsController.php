<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $comments = Comment::orderBy('updated_at', 'desc')->paginate(15);

        return view('admin.comment.index', compact('comments'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Comment $comment): RedirectResponse
    {
        if ($request->has('is_approve')) {
            $comment->is_approve ? $comment->is_approve = false : $comment->is_approve = true;
            $comment->save();
            return redirect()->back();
        }

        return redirect()->back();
    }
}
