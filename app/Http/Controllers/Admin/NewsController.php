<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class NewsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $allNews = News::orderBy('updated_at', 'desc')->paginate(15);

        return view('admin.news.index', compact('allNews'));
    }

    /**
     * @param Request $request
     * @param News $news
     * @return RedirectResponse
     */
    public function update(Request $request, News $news): RedirectResponse
    {
        $request->validate([
            'published_at' => 'nullable|date'
        ]);

        if ($request->has('published_at')) {
            is_null($news->published_at) ? $news->published_at = $request->input('published_at') : $news->published_at = null;
            $news->save();
            return redirect()->back();
        }

        return redirect()->route('admin.news.index');
    }
}
