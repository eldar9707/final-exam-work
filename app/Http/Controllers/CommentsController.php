<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\News;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;

class CommentsController extends Controller
{
    /**
     * @param News $news
     * @param CommentRequest $request
     * @return RedirectResponse
     */
    public function store(News $news, CommentRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['user_id'] = auth()->user()->id;
        $data['news_id'] = $news->id;

        Comment::create($data);

        return redirect()->back();
    }

    /**
     * @param News $news
     * @param Comment $comment
     * @param CommentRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(News $news, Comment $comment, CommentRequest $request): RedirectResponse
    {
        $this->authorize('update', $comment);
        $data = $request->validated();
        $comment->update($data);
        $comment->is_approve = false;
        $comment->save();

        return redirect()->back();
    }

    /**
     * @param News $news
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(News $news, Comment $comment): RedirectResponse
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return redirect()->back();
    }
}
