<?php

namespace App\Http\Controllers;

use App\Filters\NewsFilter;
use App\Http\Requests\NewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class NewsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index(NewsFilter $filter)
    {
        $this->validate($filter->request, [
            'tag_id' => ['int', 'exists:tags,id'],
            'category_id' => ['int', 'exists:categories,id']
        ]);

        $approveNews = News::filter($filter)->where('published_at', '<', Carbon::now())->orderBy('published_at', 'desc')->paginate(9);
        $tags = Tag::all();
        $categories = Category::all();
        return view('news.index', compact('approveNews', 'tags', 'categories'));
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', News::class);
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.create', compact('categories', 'tags'));
    }

    /**
     * @param NewsRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(NewsRequest $request): RedirectResponse
    {
        $this->authorize('create', News::class);
        $news = new News($request->validated());
        $news->save();
        if ($request->has('tags')) {
            $news->tags()->attach($request->input('tags'));
        }

        return redirect()->route('news.index');
    }

    /**
     * @param News $news
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(News $news)
    {
        $this->authorize('view', $news);

        $comments = $news->comments()->where('is_approve', true)->orderByDesc('updated_at')->paginate(5);
        return view('news.show', compact('news', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param News $news
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(News $news)
    {
        $this->authorize('update', $news);

        $categories = Category::all();
        $tags = Tag::all();
        return view('news.edit', compact('news','categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest $request
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(NewsRequest $request, News $news): RedirectResponse
    {
        $this->authorize('update', $news);

        $news->update($request->validated());
        $news->tags()->detach();
        if ($request->has('tags'))
        {
            $news->tags()->attach($request->input('tags_id'));
        }

        return redirect()->route('news.show', $news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(News $news): RedirectResponse
    {
        $this->authorize('delete', $news);
        $news->delete();
        return redirect()->back();
    }
}
