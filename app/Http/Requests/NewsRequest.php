<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use function Symfony\Component\Translation\t;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['bail', 'required', 'min:3', 'max:198', 'string'],
            'body' => ['bail', 'required', 'min:3', 'max:2048', 'string'],
            'user_id' => ['bail', 'required', 'exists:users,id'],
            'category_id' => ['exists:categories,id'],
            'tags_id' => ['exists:tags,id'],
        ];
    }
}
