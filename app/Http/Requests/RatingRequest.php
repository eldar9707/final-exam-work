<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'quality' => 'bail|required|boolean',
            'actual' => 'bail|required|boolean',
            'happy' => 'bail|required|boolean',
            'news_id' => 'bail|required|exists:news,id',
            'user_id' => 'bail|required|exists:users,id',
        ];
    }
}
