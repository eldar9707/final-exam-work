<?php

namespace App\Policies;

use App\Models\Comment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function update(User $user, Comment $comment): bool
    {
        return $comment->user->id === $user->id;
    }


    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function delete(User $user, Comment $comment): bool
    {
        return $comment->user->id === $user->id;
    }


    /**
     * @param User $user
     * @return bool|void
     */
    public function before(User $user)
    {
        if ($user->is_admin) {
            return true;
        }
    }
}
