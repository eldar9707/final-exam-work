@extends('layouts.app')
@section('content')
    @can('delete', $news)
        <a href="{{ route('news.edit', $news) }}" class="btn btn-outline-info mb-2">Редактировать</a>
        <form action="{{ route('news.destroy', $news) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-outline-danger">Удалить</button>
        </form>
    @endcan
    <div class="row row-cols-sm-1 row-cols-md-2 row-cols-lg-2">
        <div class="col col-sm-12 col-md-6 col-lg-4 row border-dark border-1 p-1">
            <div class="col-sm-12 col-md-8 col-lg-8">
                <h3>{{ $news->title }}</h3>
                @if(isset($news->category))
                    <div class="text-secondary text-uppercase">
                        <strong>Категория</strong>
                    </div>
                    <div class="badge border border-info shadow text-uppercase btn-outline-info">
                        <span class="text-dark">{{ $news->category->name }}</span>
                    </div>
                @endif

                @if($news->tags->isNotEmpty())
                    <div class="text-secondary mt-3 text-uppercase">
                        <strong>Теги</strong>
                    </div>
                    @foreach($news->tags as $tag)
                        <div class="badge border border-info shadow text-uppercase btn-outline-info">
                            <span class="text-dark">{{ $tag->name }}</span>
                        </div>
                    @endforeach
                @endif

                <div class="text-secondary mt-3 text-uppercase">
                    <strong>Автор</strong>
                </div>
                <div class="badge border border-info shadow text-uppercase btn-outline-info">
                    <span class="text-dark">{{ $news->user->name }}</span>
                </div>

                <div class="text-secondary mt-3 text-uppercase">
                    <strong>Дата создания</strong>
                </div>
                <div class="badge border border-info shadow text-uppercase btn-outline-info">
                    <span class="text-dark">{{ $news->created_at }}</span>
                </div>

                <div class="text-secondary mt-3 text-uppercase">
                    <strong>Дата публикации</strong>
                </div>
                <div class="badge border mb-3 border-info shadow text-uppercase btn-outline-info">
                    <span class="text-dark">{{ $news->published_at }}</span>
                </div>


                <form action="{{ route('rating.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="news_id" value="{{ $news->id }}">
                    <h4>Оценить новость</h4>
                    <p class="m-0">По качеству</p>
                    <div class="form-check">
                        <input class="form-check-input" value="1" type="radio" name="quality" id="quality1" checked>
                        <label class="form-check-label" for="quality1">
                            Качественно
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" value="0" type="radio" name="quality" id="quality2">
                        <label class="form-check-label" for="quality2">
                            Некачественно
                        </label>
                    </div>

                    <hr>
                    <p class="m-0">По актуальности</p>
                    <div class="form-check">
                        <input class="form-check-input" value="1" type="radio" name="actual" id="actual1" checked>
                        <label class="form-check-label" for="actual1">
                            Актуально
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" value="0" type="radio" name="actual" id="actual0">
                        <label class="form-check-label" for="actual0">
                            Не Актуально
                        </label>
                    </div>

                    <hr>
                    <p class="m-0">По довольности</p>
                    <div class="form-check">
                        <input class="form-check-input" value="1" type="radio" name="happy" id="happy1" checked>
                        <label class="form-check-label" for="happy1">
                            Доволен
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" value="0" type="radio" name="happy" id="happy2">
                        <label class="form-check-label" for="happy2">
                            Не Доволен
                        </label>
                    </div>
                    <button type="submit" class="btn btn-outline-info fw-bold shadow-lg">Оценить</button>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-8 border-dark p-1 border-1">
            <div>
                <h4>Текст новости</h4>
                <p>{{ $news->body }}</p>
            </div>
        </div>
    </div>

    @if($comments->count() > 0)
        <div class="list-group">
            <h3 class="my-3">Количество комментариев: {{ $comments->count() }}</h3>
            @foreach($comments as $comment)
                <div class="list-group-item list-group-item-action shadow-lg">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="w-75">
                            <h5 class="mb-1">
                                <a href="" class="text-decoration-none">{{ $comment->user->name }}</a>
                            </h5>
                            <p class="mb-1"><em>{{ $comment->message }}</em></p>
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <div id="flush-collapse{{ $comment->id }}" class="accordion-collapse collapse"
                                         aria-labelledby="flush-heading"
                                         data-bs-parent="#accordionFlushExample">
                                        <div class="accordion-body">
                                            @can('delete', $comment)
                                                <form
                                                    action="{{ route('news.comments.update', ['news' => $news, 'comment' => $comment]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="row">
                                                        <div class="input-group">
                                                        <textarea class="form-control border-info"
                                                                  name="message"
                                                                  aria-label="With textarea"
                                                                  cols="30" rows="5"
                                                        >{{ $comment->message }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-2 mb-5">
                                                        <div class="col-12">
                                                            <button type="submit"
                                                                    class="btn btn-outline-info">Редактировать
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endcan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">{{ $comment->updated_at->diffForHumans() }}</small>
                            @can('delete', $comment)
                                <form
                                    action="{{ route('news.comments.destroy', ['news' => $news, 'comment' => $comment]) }}"
                                    method="POST" class="text-end">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-outline-info btn-sm" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapse{{ $comment->id }}" aria-expanded="false"
                                            aria-controls="flush-collapse{{ $comment->id }}">
                                        Редактировать
                                    </button>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">
                                        Удалить
                                    </button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row justify-content-md-center mt-5">
            <div class="col-md-auto">
                {{ $comments->withQueryString()->links('pagination::bootstrap-4') }}
            </div>
        </div>
    @endif

    @auth()
        <div>
            <h3 class="mt-5 mb-3">Добавить коммент</h3>
            <form action="{{ route('news.comments.store', $news) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="input-group">
                            <textarea
                                class="form-control border-info @error('message') is-invalid border-danger @enderror"
                                name="message" aria-label="With textarea"
                                cols="30" rows="5" placeholder="Напишите коммент"></textarea>
                    </div>
                    @error('message')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>
                <div class="row mt-2 mb-5">
                    <div class="col-12">
                        <button type="submit" class="btn btn-outline-info">Добавить</button>
                    </div>
                </div>
            </form>
        </div>
    @endauth
    @guest()
        <h4><a class="btn btn-outline-success"
               href="{{ route('login') }}">Войти</a> Что бы оставить комментарий</h4>
    @endguest

    <a href="{{route('news.index')}}" class="btn btn-outline-info">@lang('messages.go_back')</a>
@endsection
