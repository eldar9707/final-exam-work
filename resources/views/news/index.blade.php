@extends('layouts.app')
@section('content')
    @if(Auth::user() && Auth::user()->is_admin)
        <a href="{{ route('admin.news.index') }}" class="btn btn-success mb-5">АДМИНКА</a>
        <a href="{{ route('news.create') }}" class="btn btn-outline-info mb-5">Добавить новость</a>
    @endif
    <div class="mb-5">
        <form id="form_id" action="{{ route('news.index') }}" method="GET">
            <h5>Фильтрация по тегам</h5>
            <select name="tag_id" class="form-select" aria-label="Default select example" onchange="this.form.submit()">
                <option selected disabled>Отфильтровать по тегам:</option>
                @foreach($tags as $tag)
                    <option @if(isset($_GET['tag_id'])) @if($_GET['tag_id'] == $tag->id)
                            selected @endif @endif value="{{$tag->id}}">{{$tag->name}}</option>
                @endforeach
            </select>
        </form>
    </div>

    <div class="mb-5">
        <form id="category_id" action="{{ route('news.index') }}" method="GET">
            <h5>Фильтрация по категориям</h5>
            <select name="category_id" class="form-select" aria-label="Default select example"
                    onchange="this.form.submit()">
                <option selected disabled>Отфильтровать по категориям:</option>
                @foreach($categories as $category)
                    <option @if(isset($_GET['category_id'])) @if($_GET['category_id'] == $category->id)
                            selected @endif @endif value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </form>
    </div>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($approveNews as $news)
            <div class="col mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ route('news.show', $news) }}">{{ $news->title }}</a></h5>
                        <p class="card-text">{{ $news->body }}</p>
                        <div class="col-sm-12 col-md-8 col-lg-8">
                            @if($news->tags->isNotEmpty())
                                <div class="text-secondary mt-3 text-uppercase">
                                    <strong>ТЕГИ</strong>
                                </div>
                                @foreach($news->tags as $tag)
                                    <div class="badge border border-info shadow text-uppercase btn-outline-info">
                                        <a href="#" class="text-dark text-decoration-none">{{ $tag->name }}</a>
                                    </div>
                                @endforeach
                            @endif
                            @if(isset($news->category))
                                <div class="text-secondary mt-3 text-uppercase">
                                    <strong>КАТЕГОРИЯ</strong>
                                </div>
                                <div class="badge border border-info shadow text-uppercase btn-outline-info">
                                    <a href="#" class="text-dark text-decoration-none">{{ $news->category->name }}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">
                            <strong>Дата публикации:</strong> {{ $news->published_at->diffForHumans() }}
                        </small>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $approveNews->withQueryString()->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
