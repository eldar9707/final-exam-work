@extends('layouts.app')

@section('content')
    <div class="col main pt-5 mt-3">
        <h1>Создать новую новость</h1>
        <form method="post" action="{{ route('news.store') }}">
            @csrf
            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
            <div class="form-group">
                <label for="title"><b>Фамилия, имя</b></label>
                <input placeholder="Введите заголовок" autofocus type="text" class="form-control" id="title"
                       name="title">
                @error('title')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group mt-3">
                <label for="floatingTextarea2"><b>Новость</b></label>
                <div class="form-floating">
                    <textarea class="form-control" name="body" id="floatingTextarea2" style="height: 100px"></textarea>
                </div>
                @error('body')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror
            </div>
            <div class="form-group mt-3">
                <label for="categories"><b>Категории</b></label>
            <select name="category_id" id="categories" class="form-select" aria-label="Default select example">
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            </div>
            <p>
                <a class="btn btn-outline-info dropdown-toggle mt-3" data-bs-toggle="collapse" href="#tags"
                   role="button" aria-expanded="false" aria-controls="collapseExample">
                    Теги
                </a>
            </p>
            <div class="collapse" id="tags">
                <div class="card card-body">
                    @foreach($tags as $tag)
                        <div class="form-check">
                            <input class="form-check-input" name="tags[]" type="checkbox"
                                   value=" {{ $tag->id }} " id="tags{{$tag->id}}">
                            <label class="form-check-label" for="tags{{$tag->id}}">
                                {{$tag->name}}
                            </label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button type="submit" class="btn btn-primary mt-4">Сохранить</button>
        </form>
    </div>
@endsection
