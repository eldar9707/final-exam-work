@extends('layouts.app')

@section('content')
    <h3>Все Новости</h3>
    <div class="table-responsive overflow-scroll">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th scope="col">Новость</th>
                <th scope="col">Дата создания</th>
                <th scope="col">Дата публикации</th>
                <th scope="col" class="text-end">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($allNews as $news)
                <tr class="align-middle">
                    <td>{{ $news->title }}</td>
                    <td>{{ $news->created_at }}</td>
                    <td>{{is_null($news->published_at) ? 'Не опубликован' : $news->published_at}}</td>
                    <td class="text-end">
                        <div class="btn-group">
                            <div>
                                <a href="{{ route('news.edit', $news) }}" type="button"
                                   class="btn btn-sm btn-outline-info">Изменить</a>
                            </div>

                            <form action="{{ route('admin.news.update', $news) }}" method="post">
                                @method('PUT')
                                @csrf
                                @if(!is_null($news->published_at))
                                    <input type="hidden" name="published_at">
                                    <button class="btn btn-sm btn-success" type="submit">Снять с публикации</button>
                                @else
                                    <a class="btn btn-sm btn-outline-success" data-bs-toggle="modal" data-bs-target="#exampleModal">Опубликовать
                                    </a>
                                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Дата Публикации</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <input class="form-control" name="published_at" type="datetime-local"/>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрыть</button>
                                                    <button type="submit" class="btn btn-primary">Сохранить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </form>
                            <form action="{{ route('news.destroy', $news) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-sm btn-outline-danger" type="submit">Удалить</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $allNews->withQueryString()->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
