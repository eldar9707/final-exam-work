@extends('layouts.app')

@section('content')
    <h3>Все Комменты</h3>
    <div class="table-responsive overflow-scroll">
        <table class="table table-striped table-hover table-sm">
            <thead>
            <tr>
                <th scope="col">Коммент</th>
                <th scope="col">Новость</th>
                <th scope="col">Пользователь</th>
                <th scope="col" class="text-end">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($comments as $comment)
                <tr class="align-middle">
                    <td>{{ $comment->message }}</td>
                    <td><a href="{{ route('news.show', $comment->news) }}">{{ $comment->news->title }}</a></td>
                    <td>{{ $comment->user->name }}</td>
                    <td class="text-end">
                        <div class="btn-group" role="group">
                            <form action="{{route('admin.comments.update', $comment)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="is_approve">
                                @if($comment->is_approve)
                                    <button class="btn btn-sm btn-success" type="submit">Снять подтверждение</button>
                                @else
                                    <button class="btn btn-sm btn-outline-success" type="submit">Подтвердить</button>
                                @endif
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="row justify-content-md-center p-5">
        <div class="col-md-auto">
            {{ $comments->withQueryString()->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection
