<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $tags = [
            ['name' => 'Юмор'],
            ['name' => 'Убийство'],
            ['name' => 'Политика'],
            ['name' => 'Спорт'],
            ['name' => 'Учеба'],
        ];

        foreach ($tags as $tag) {
            Tag::create($tag);
        }
    }
}
