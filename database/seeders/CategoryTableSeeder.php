<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Интернет'],
            ['name' => 'Культура'],
            ['name' => 'Наука и технологии'],
            ['name' => 'Общество'],
            ['name' => 'Политика'],
            ['name' => 'Преступность и право'],
            ['name' => 'Происшествия'],
            ['name' => 'Спорт'],
            ['name' => 'Экономика'],
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
