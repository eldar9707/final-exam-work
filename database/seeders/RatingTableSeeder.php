<?php

namespace Database\Seeders;

use App\Models\News;
use App\Models\Rating;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RatingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 15; $i++) {
            Rating::factory()
                ->for(User::all()->random())
                ->for(News::where('published_at', '<', Carbon::now())->get()->random())
                ->create();
        }
    }
}
