<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\News;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 30; $i++) {
            Comment::factory()
                ->for(User::all()->random())
                ->for(News::all()->random())
                ->create();
        }
    }
}
