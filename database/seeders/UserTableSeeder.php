<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run()
    {
        User::factory()->count(20)->create();
        $user_admin = User::first();
        $user_admin->name = 'Admin Adminov';
        $user_admin->email = 'admin@admin.com';
        $user_admin->is_admin = true;
        $user_admin->save();
    }
}
