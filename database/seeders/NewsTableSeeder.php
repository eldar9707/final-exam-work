<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 40; $i++) {
            News::factory()
                ->for(User::all()->random())
                ->for(Category::all()->random())
                ->hasAttached(Tag::all()->random(rand(1, 5)))
                ->create();
        }
    }
}
