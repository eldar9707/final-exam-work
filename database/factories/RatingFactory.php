<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'quality' => rand(0, 1),
            'actual' => rand(0, 1),
            'happy' => rand(0, 1),
        ];
    }
}
