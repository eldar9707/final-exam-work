<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * @return array
     */
    public function definition(): array
    {
        return [
            'message' => $this->faker->realText,
            'is_approve' => rand(0, 1),
        ];
    }
}
