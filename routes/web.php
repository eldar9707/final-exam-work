<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\NewsController::class, 'index']);

Route::resource('news', \App\Http\Controllers\NewsController::class);

Route::post('ratings', [\App\Http\Controllers\RatingController::class, 'store'])
    ->name('rating.store')
    ->middleware('auth');

Route::resource('news.comments', \App\Http\Controllers\CommentsController::class)
    ->only(['store', 'update', 'destroy'])
    ->middleware('auth');

Route::prefix('admin')->name('admin.')->middleware(['auth', 'admin'])->group(function (){
    Route::resource('news', \App\Http\Controllers\Admin\NewsController::class)
        ->only('index', 'update');

    Route::resource('comments', \App\Http\Controllers\Admin\CommentsController::class)
        ->only('index', 'update');
});

Auth::routes();
